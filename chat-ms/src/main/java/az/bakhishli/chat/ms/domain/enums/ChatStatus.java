package az.bakhishli.chat.ms.domain.enums;

public enum ChatStatus {
    JOIN,
    MESSAGE,
    LEAVE
}
