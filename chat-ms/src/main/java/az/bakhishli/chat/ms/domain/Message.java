package az.bakhishli.chat.ms.domain;

import az.bakhishli.chat.ms.domain.enums.ChatStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Message {
    private String sender;
    private String receiverName;
    private String message;
    private String date;
    private ChatStatus status;
}
